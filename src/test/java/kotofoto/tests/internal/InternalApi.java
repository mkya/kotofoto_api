package kotofoto.tests.internal;

import io.qameta.allure.Epic;
import kotofoto.data.KotofotoData;
import kotofoto.helpers.BaseHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

@Test(groups = "Api")
public class InternalApi extends BaseHelper {
    @Epic(value = "Login pass")
    @Test()
    public void loginPass() {
        Assert.assertTrue(get("/lk/").getBody().asString().contains("Максим"));
    }

    @Epic(value = "Проверка текста")
    @Test(dataProvider = "internalData", groups = {"restContent"}, dataProviderClass = KotofotoData.class)
    public void checkData(String text) {
        Assert.assertTrue(get("/page/dostavka.html").getBody().asString().contains(text));
        //Но обычно проверяем json из resources через JsonAssert.assertJsonEquals
    }
}
