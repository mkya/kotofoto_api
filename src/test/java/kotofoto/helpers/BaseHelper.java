package kotofoto.helpers;

import io.restassured.http.ContentType;
import io.restassured.http.Cookies;
import io.restassured.response.Response;
import org.testng.Assert;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import static io.restassured.RestAssured.given;

public class BaseHelper {
    protected static final String webUrl = "https://kotofoto.ru";
    private static final String intApi = "/class/api.php?act=loginkotofoto";
    protected static final String RESOURCES_PATH = "src/test/resources/";

    Cookies cookies = null;

    public BaseHelper() {
        Properties appProperties = new Properties();
        try {
            appProperties.load(new FileReader("src/test/resources/app.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        cookies = given()
                .formParam("mail", appProperties.getProperty("userLogin"))
                .formParam("pass", appProperties.getProperty("userPassword"))
                .accept(ContentType.URLENC)
                .post(webUrl + intApi).then()
                .extract().response().getDetailedCookies();
    }

    public Response get(String path) {
        Response response = given().cookies(cookies)
                .contentType(ContentType.URLENC)
                .when()
                .post(webUrl + path)
                .then().extract().response();
        Assert.assertEquals(response.getStatusCode(), 200, "Bad status\n" + response.body().asString());
        return response;
    }
}
